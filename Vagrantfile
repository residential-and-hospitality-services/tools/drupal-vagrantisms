# You will need vagrant-hostsupdater for this to work correctly. Install it with:
#
#   vagrant plugin install vagrant-hostsupdater

# Given the proliferation of gTLDs, please note that the domain names here ought to be in one of the reserved TLDs
# from RFC 2606.
domain = "drupal.test"
ip = "192.168.20.75"

Vagrant.configure(2) do |config|
    config.vm.box = "ubuntu/trusty32"

    config.vm.network "private_network", ip: ip

    config.vm.hostname = domain
    config.hostsupdater.aliases = Dir.open("sites").select { |f| f[/\.tgz$/] }
                                                   .collect { |f| f.sub(/\.tgz$/, ".#{domain}") }

    config.vm.synced_folder ".", "/vagrant", disabled: true
    config.vm.synced_folder "sites", "/vagrant/sites", mount_options: ["ro"]
    config.vm.synced_folder "deploy", "/var/www/html", create: true, owner: "vagrant", group: "www-data",
                                      mount_options: ["dmode=770"]
    config.vm.synced_folder "provision/config", "/vagrant/config", mount_options: ["ro"]

    config.vm.provider "virtualbox" do |vb|
        vb.customize ["modifyvm", :id, "--memory", "2048"]
    end

    config.vm.provision "update_package_list", type: "shell", path: "provision/update_package_list.sh"
    config.vm.provision "tools", type: "shell", path: "provision/tools.sh"
    config.vm.provision "inputrc", type: "shell", path: "provision/inputrc.sh"
    config.vm.provision "amp_stack", type: "shell", path: "provision/amp_stack.sh"
    config.vm.provision "composer", type: "shell", path: "provision/composer.sh"
    config.vm.provision "drush", type: "shell", path: "provision/drush.sh", args: ["8.1.16"]
    config.vm.provision "deploy", type: "shell", path: "provision/deploy.sh", args: [domain, ip]
end
