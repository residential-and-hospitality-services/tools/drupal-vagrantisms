# Drupal Vagrantisms

Drupal-vagrantisms is a Vagrant configuration and bootstrap script for rapidly
provisioning a test environment for Drupal sites loaded from `drush` archives.

## Usage

Clone this project, drop all your Drupal archives under `sites/`, and run
`vagrant up`.

Note: Site dumps must be named `$sitename.tgz`.

## Installation

You will need vagrant-hostsupdater for this to work correctly. Install it with:

```sh
vagrant plugin install vagrant-hostsupdater
```
