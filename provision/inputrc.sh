set -euo pipefail

sed 's/^ \{4\}//' <<INPUTRC >/etc/inputrc
    set match-hidden-files off
    set revert-all-at-newline on
    set editing-mode vi

    set keymap vi-insert
    "\C-b": unix-filename-rubout
    "\C-l": clear-screen

    set keymap vi-command
    "\C-b": unix-filename-rubout
    "\C-l": clear-screen
    "\C-a": yank-nth-arg

    set bell-style none
INPUTRC
