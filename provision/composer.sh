declare tmp=

quit() {
    trap "" INT QUIT TERM HUP PIPE USR1 USR2
    trap -- EXIT
    [[ -n ${1-} ]] && echo "Error: $1, quitting!" >&2
    [[ -f $tmp ]] && rm -- "$tmp"
    if [[ -n ${1-} ]]; then exit 1; else exit 0; fi
}

trap quit EXIT
trap 'quit "Uncaught error at $(basename -- "$0"):$LINENO"' ERR
set -Euo pipefail

echo "Installing Composer..."

tmp=$(mktemp)
curl -s https://getcomposer.org/installer >"$tmp"
php -f "$tmp" -- --quiet --install-dir=/usr/local/bin --filename composer
