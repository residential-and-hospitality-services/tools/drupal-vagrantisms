declare tmp= sites=()

quit() {
    trap "" INT QUIT TERM HUP PIPE USR1 USR2
    trap -- EXIT
    [[ -n ${1-} ]] && echo "Error: $1, quitting!" >&2
    [[ -n ${tmp-} ]] && rm -rf -- "$tmp"
    if [[ -n ${1-} ]]; then exit 1; else exit 0; fi
}

trap quit EXIT
trap 'quit "Uncaught error at line $LINENO"' ERR
set -Euo pipefail
shopt -s nullglob

source /vagrant/config/settings.sh

domain=$1
ip=$2

#
# Unpack sites.
#

for archive in /vagrant/sites/*.tgz; do
    site=$(basename $archive); site=${site%.tgz}
    hostname=$site.$domain
    newly_extracted=0
    database_exists=0

    #
    # Verify archive and extract site files.
    #

    if [[ ! -d $WEB_TARGET/$site ]]; then
        echo "Extracting Drupal site $site..."

        if ! (set +o pipefail; tar -tf "$archive" | grep -qFx -e "$site/"); then
            echo "Malformed site archive (expecting subdirectory named $site)." >&2
            echo "Skipping extraction and configuration." >&2
            continue
        fi

        conflicts=$(tar -tf "$archive" | sort -f | uniq -i --all-repeated=separate)
        if [[ -n $conflicts ]]; then
            sed 's/^ \{16\}//' <<NOTICE
                Warning! Drupal site $site contains files whose names differ only in
                letter case:

                $(echo "$conflicts" | sed 's/^/    /')

                Many filesystems do not permit such files to co-exist, notably the
                default filesystems created by Microsoft Windows and OS X. If this
                Vagrant instance is running on a non-case-sensitive filesystem, only one
                file from each set shown above will be kept, and your site may
                malfunction. Usually, such files are created by mistake: the same script
                may have been downloaded and saved as someStuff.js and somestuff.js. You
                should consider eliminating the duplicates upstream. To identify these
                files, use uniq:

                    find [<location>] | sort -f | uniq -iD

NOTICE
        fi

        # We extract the files to a temporary directory because the vboxsf filesystem doesn't support hard links, and
        # tar does not have an option to extract hard links as separate files.

        tmp=$(mktemp -d)

        tar -C "$tmp" -xf "$archive" -- "$site"

        # Break hard links.
        find "$tmp" -type f -links +1 -exec cp --preserve=mode,ownership,timestamps,xattr -- {} "$tmp/f" \; \
            -exec mv -- "$tmp/f" {} \;

        if ! mv -- "$tmp/$site" "$WEB_TARGET"; then
            if [[ -n $conflicts ]]; then
                echo "mv failed, likely for the reasons given above. Continuing anyway." >&2
            else
                echo "mv failed. Skipping extraction and configuration of Drupal site $site." >&2
                rm -rf -- "$tmp"
                unset tmp
                continue
            fi
        fi
        rmdir -- "$tmp"
        unset tmp

        newly_extracted=1
    else
        echo "Skipping extraction of Drupal site $site: already deployed."
    fi

    #
    # Create and load database.
    #

    mysql -N <<<"SHOW DATABASES" | grep -qFx -e "$site" && database_exists=1

    if ((database_exists && newly_extracted)); then
        echo "Dropping existing database."
        sed 's/^ \{12\}//' <<SQL | mysql
            GRANT USAGE ON *.* TO '$site'@'localhost';
            DROP USER '$site'@'localhost';
            DROP DATABASE IF EXISTS \`$site\`;
SQL
        database_exists=0
    fi

    if ((!database_exists)); then
        # Random password for site database.
        password=$(dd if=/dev/urandom bs=12 count=1 status=none | openssl base64 | tr / _)

        sed 's/^ \{12\}//' <<SQL | mysql
            CREATE DATABASE \`$site\`;
            CREATE USER '$site'@'localhost' IDENTIFIED BY '$password';
            GRANT ALL PRIVILEGES ON \`$site\`.* TO '$site'@'localhost';
            FLUSH PRIVILEGES;
SQL

        mapfile -t dumps < <(tar -tf "$archive" | grep -x "[^/]\\+\\.sql" &&:)
        if ((!${#dumps[@]})); then
            echo "No database dump found." >&2
        fi
        for dump in "${dumps[@]}"; do
            echo "Loading database file $dump..."
            tar -Oxf "$archive" "$dump" | mysql -u"$site" -p"$password" "$site"
        done

        if [[ -f $WEB_TARGET/$site/sites/default/settings.php ]]; then
            sed "s/\$site/$site/g;s/\$password/$password/g" </vagrant/config/settings.php.template \
                >>"$WEB_TARGET/$site/sites/default/settings.php"
        else
            echo "Could not find settings file sites/default/settings.php for site $site." >&2
            sed 's/^ \{16\{//' <<NOTICE
                You should configure the site to use these database parameters:

                    Service:  MySQL
                    Host:     localhost
                    Username: $site
                    Password: $password
NOTICE
        fi
    fi

    #
    # Run post-deployment tasks.
    #

    cd -- "$WEB_TARGET/$site"
    if ((newly_extracted)); then
        echo "Rebuilding Drupal cache..."
        drush cache-rebuild --quiet || echo "drush exited with non-zero status $?." >&2

        if [[ -f composer.json ]]; then
            echo "Installing Composer dependencies..."
            composer install --quiet || echo "composer exited with non-zero status $?." >&2
        fi
    fi

    #
    # Add hosts entry.
    #

    grep -qF "$ip $hostname" /etc/hosts || echo "$ip $hostname" >>/etc/hosts

    #
    # Configure Apache.
    #

    sed -e "s/\$site/$site/g" -e "s/\$hostname/$hostname/g" </vagrant/config/apache.conf.template \
        >"/etc/apache2/sites-available/$site.conf"

    if [[ ! -f /etc/apache2/ssl/$site.key || ! -f /etc/apache2/ssl/$site.crt ]]; then
        umask 077
        mkdir -p /etc/apache2/ssl
        openssl req -x509 -subj "/CN=$hostname/O=Testing/C=US" -days 36500 \
            -newkey rsa -nodes -keyout "/etc/apache2/ssl/$site.key" -out "/etc/apache2/ssl/$site.crt" 2>/dev/null
        umask 022
    fi

    a2ensite "$site" >/dev/null
    sites+=($site)
done
apachectl restart >/dev/null

#
# Create and display motd.
#

echo "This machine is hosting the following sites:" >/etc/motd
for site in ${sites[@]+"${sites[@]}"}; do
    echo "    http://$site.$domain" >>/etc/motd
    echo "    https://$site.$domain" >>/etc/motd
done

cat /etc/motd
