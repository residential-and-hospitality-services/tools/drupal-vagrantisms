trap 'echo "Uncaught error at line $LINENO" >&2; exit 1' ERR
set -Euo pipefail

source /vagrant/config/settings.sh

if [[ ! -f /root/.my.cnf ]]; then
    echo "Generating random password for MySQL root account..."
    mysql_root_password=$(dd if=/dev/urandom bs=12 count=1 status=none | openssl base64)

    umask 077
    sed 's/^ \{8\}//' <<MYCNF | tee {/root,/home/$USER}/.my.cnf >/dev/null
        [client]
        host = localhost
        user = root
        password = $mysql_root_password
MYCNF
    chown $USER: /home/$USER/.my.cnf
    umask 022

    debconf-set-selections <<<"mysql-server mysql-server/root_password password $mysql_root_password"
    debconf-set-selections <<<"mysql-server mysql-server/root_password_again password $mysql_root_password"
fi

echo "Installing LAMP stack and associated tools..."
debconf-set-selections <<<"phpmyadmin phpmyadmin/internal/skip-preseed boolean true"
debconf-set-selections <<<"phpmyadmin phpmyadmin/reconfigure-webserver multiselect"
debconf-set-selections <<<"phpmyadmin phpmyadmin/dbconfig-install boolean false"

apt-get --yes install lamp-server^ php5-curl phpmyadmin &>/dev/null
sed -i -e "s/^export APACHE_RUN_USER=www-data/export APACHE_RUN_USER=$USER/" /etc/apache2/envvars
service apache2 restart
a2enmod rewrite >/dev/null
a2enmod ssl >/dev/null
a2dissite 000-default >/dev/null

cp /vagrant/config/apache.phpmyadmin.conf /etc/apache2/sites-available/phpmyadmin.conf
a2ensite phpmyadmin >/dev/null
