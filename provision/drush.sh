URL="https://github.com/drush-ops/drush/releases/download/%s/drush.phar" 

declare tmp=

quit() {
    trap "" INT QUIT TERM HUP PIPE USR1 USR2
    trap -- EXIT
    [[ -n ${1-} ]] && echo "Error: $1, quitting!" >&2
    [[ -f ${tmp-} ]] && rm -- "$tmp"
    if [[ -n ${1-} ]]; then exit 1; else exit 0; fi
}

trap quit EXIT
trap 'quit "Uncaught error at $(basename -- "$0"):$LINENO"' ERR
set -Euo pipefail

version=$1

echo "Installing Drush..."

tmp=$(mktemp)
status=$(curl -sL -o /dev/stderr -w "%{http_code}" "$(printf "$URL" "$version")" 2>"$tmp")
((status == 200)) || quit "Download failed"
mv -- "$tmp" /usr/local/bin/drush
unset tmp
chmod 0755 /usr/local/bin/drush
